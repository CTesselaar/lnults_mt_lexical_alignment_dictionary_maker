﻿using System;
using System.Collections.Generic;
using System.Text;
using C5;
using System.Text.RegularExpressions;

namespace BuildAlignmentDictionary
{
    class DictionaryBuilder

    {
        public static Regex FILTER = new Regex(@"^(([0-9])+([.])?$|^([0-9])+([,./-:;\\]([0-9])+)+|(,|/|\.|<|>|-|[@]|^|[+]|[!]|[*]|;|:|µ|'|""|%|&|\$)+|NULL|[0-9]+([.,':;""?/]*[A-Za-z]+[0-9]*)+|[A-Za-z]+([.,]*[0-9]+[A-Za-z]*)+)$", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        public static double lexscore_threshold = 0.005;

        /// <summary>
        /// Reads SMT lexical table to build dictionary to be used in our aligner
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="write2output"></param>
        /// <returns></returns>
        /// C5.KeyValuePair<string, List<Tuple<string, double>>>
        //TODO: The filter also filters out zwei-2 pairs, add these in mannually (will only be important for <4, add manually)
        private static TreeDictionary<string, List<Tuple<string, double>>> MakeDictionary2(string filename, bool write2output, string ext)
        {
            System.IO.StreamReader file = new System.IO.StreamReader(filename);
            string outfile = filename.Replace(".1.", ext);
            System.IO.StreamWriter output = new System.IO.StreamWriter(outfile, false);



            TreeDictionary<String, List<Tuple<string, double>>> treedict = new TreeDictionary<String, List<Tuple<string, double>>>();
            String line;
            int count = 0;
            string CurrKey = "";
            string PrevKey = "";
            List<Tuple<string, double>> currTranslations = new List<Tuple<string, double>>();

            // Line level operations
            while ((line = file.ReadLine()) != null)
            {
                count++;
                String[] splitline = line.Split(' ');

                // Filter out non-words: numbers, references, NULL etc.
                if (FILTER.Matches(splitline[0]).Count != 0 | FILTER.Matches(splitline[1]).Count != 0)
                {
                    Console.WriteLine("Reject: " + splitline[0] + " " + splitline[1]);
                    continue;
                }


                // New keys: add to the treedictionary
                if (CurrKey.Equals(""))
                {
                    PrevKey = CurrKey;
                    CurrKey = splitline[0];

                    // New word: add value to list                   
                    var tuple = new Tuple<string, double>(splitline[1], Convert.ToDouble(splitline[2]));
                    if (Convert.ToDouble(splitline[2]) > lexscore_threshold)
                    {
                        currTranslations.Add(tuple);
                    }


                }
                else if (!CurrKey.Equals(splitline[0]))
                {
                    // Next word in list is reached

                    // First write the current key / list of translations to file
                    string list = "";

                    // If there were any translations above the threshold
                    // TODO: "best guess translation": if trans. list is empty, keep the one highest translation
                    if (currTranslations.Count != 0)
                    {
                        for (int i = 0; i < currTranslations.Count; i++)
                        {
                            // Item 1: only the translation, no lexical score (=Item2)
                            if (i == 0)
                            {
                                list += currTranslations[i].Item1;
                            }
                            else
                            {
                                list = list + "," + currTranslations[i].Item1;
                            }

                            // list = list + item.ToString();
                        }
                        output.Write(CurrKey + "\t" + list + "\n");
                    }

                    // Update key, reset list and add first translation entry
                    PrevKey = CurrKey;
                    CurrKey = splitline[0];

                    // Add it to the dictionary (no sort for new entries)                   
                    var tuple = new Tuple<string, double>(splitline[1], Convert.ToDouble(splitline[2]));
                    currTranslations.Clear();
                    if (Convert.ToDouble(splitline[2]) > lexscore_threshold)
                    {
                        currTranslations.Add(tuple);
                    }


                }
                else if (CurrKey.Equals(splitline[0]))
                {
                    //If not new: add this translation to the existing translations list for this key and sort   
                    if (Convert.ToDouble(splitline[2]) > lexscore_threshold)
                    {
                        currTranslations.Add(new Tuple<string, double>(splitline[1], Convert.ToDouble(splitline[2])));
                        currTranslations.Sort((x, y) => y.Item2.CompareTo(x.Item2));
                    }



                }



            }

            output.Flush();
            output.Close();



            Console.WriteLine("Finished building dictionary.");
            return treedict;
        }
    }
}
